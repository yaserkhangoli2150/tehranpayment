import {RouteProp} from '@react-navigation/native';
import type {NativeStackScreenProps} from '@react-navigation/native-stack';

export type MainStackNavigatorParamList = {
  Landing: undefined;
};

export type MainNavigationProp = NativeStackScreenProps<
  MainStackNavigatorParamList,
  Landing
>;

export type RootRouteProps<
  RouteName extends keyof MainStackNavigatorParamList,
> = RouteProp<MainStackNavigatorParamList, RouteName>;
