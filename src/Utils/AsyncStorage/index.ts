import AsyncStorage from '@react-native-async-storage/async-storage';

// Storing string value
const storeStringData = async (key: string, value: string) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {}
};

// Storing boolean value
const storeBooleanData = async (key: string, value: boolean) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (e) {}
};

//   Storing object value
const storeObjectData = async (key: string, value: any) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {}
};

// Reading string value
const getStringData = async (key: string) => {
  try {
    const value = await AsyncStorage.getItem(key);
    return value;
  } catch (e) {}
};

// Reading object value
const getObjectData = async (key: string) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {}
};

// Reading boolean value
const getBooleanData = async (key: string) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : false;
  } catch (e) {}
};

// remove item
const removeValue = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    // remove error
  }
};

// Clear async storage
const clearAsyncStorage = async () => {
  try {
    await AsyncStorage.clear();
  } catch (error) {}
};

export default {
  storeStringData,
  storeObjectData,
  storeBooleanData,
  getStringData,
  getObjectData,
  getBooleanData,
  removeValue,
  clearAsyncStorage,
};
