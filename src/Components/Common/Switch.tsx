import {Switch, ViewProps} from 'react-native';
import React from 'react';
import {ThemeContext} from '@contexts/ThemeContext';

interface ISwitchType extends ViewProps {
  onValueChange: () => void;
  value: boolean;
  disabled?: boolean;
}

const ISwitch = (props: ISwitchType) => {
  const {selectedTheme} = React.useContext(ThemeContext);
  return (
    <Switch
      {...props}
      disabled={props.disabled}
      trackColor={{
        false: selectedTheme.colors.disable_outer,
        true: selectedTheme.colors.disable_outer,
      }}
      thumbColor={
        props.value
          ? selectedTheme.colors.primary
          : selectedTheme.colors.disable_inner
      }
      onValueChange={props.onValueChange}
      value={props.value}
    />
  );
};

export default ISwitch;
