import React, {memo} from 'react';
import {StyleSheet, TouchableOpacity, ViewStyle} from 'react-native';
import Animated, {
  runOnJS,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import CardView from './CardView';
import {IconChange} from '@assets/icons';

interface SwitchProps {
  value: boolean;
  onChange: (value: boolean) => void;
  size?: 'sm' | 'md' | 'lg';
  activeBackgroundColor?: string;
  style?: ViewStyle;
}

const Swap = ({value, onChange}: SwitchProps) => {
  const switchValue = useSharedValue<boolean>(value);
  const toggleSwitch = () => {
    switchValue.value = !switchValue.value;
    onChange && runOnJS(onChange)(switchValue.value);
  };

  const knobStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateX: withTiming(switchValue.value ? 113 : 5),
        },
      ],
    };
  });

  return (
    <TouchableOpacity activeOpacity={0.8} onPress={toggleSwitch}>
      <CardView style={styles.switch}>
        <Animated.View style={[styles.knob, knobStyle]}>
          <IconChange />
        </Animated.View>
      </CardView>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  switch: {
    justifyContent: 'center',
    height: 60,
    width: 160,
    borderRadius: 14,
    padding: 0,
  },
  knob: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 40,
    borderRadius: 20,
    backgroundColor: '#4DACF9',
  },
});

export default memo(Swap);
