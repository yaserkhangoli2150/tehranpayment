import {View, ViewProps, ViewStyle} from 'react-native';
import React, {ReactNode} from 'react';
import {ThemeContext} from '@contexts/ThemeContext';
import {IBorderType, IColorType} from '@theme/Color';

interface ViewInterface extends React.ComponentProps<typeof View> {
  borderColor?: IBorderType;
  backgroundColor?: IColorType;
}

interface ICardInterface extends ViewProps {
  children: ReactNode;
  style?: ViewInterface & ViewStyle;
}

const CardView = (props: ICardInterface) => {
  const {selectedTheme} = React.useContext(ThemeContext);

  return (
    <View
      {...props}
      style={[
        props.style,
        {
          borderWidth:
            props.style?.borderWidth || props.style?.borderWidth == 0
              ? props.style?.borderWidth
              : 1,
          borderColor:
            selectedTheme.colors[props.style?.borderColor as any] ||
            selectedTheme.colors.border,
          padding:
            props.style?.padding || props.style?.padding == 0
              ? props.style?.padding
              : 12,
          borderRadius:
            props.style?.borderRadius || props.style?.borderRadius == 0
              ? props.style?.borderRadius
              : 4,
          backgroundColor:
            selectedTheme.colors[props.style?.backgroundColor as any] ||
            selectedTheme.colors.card,
        },
      ]}>
      {props.children}
    </View>
  );
};

export default CardView;
