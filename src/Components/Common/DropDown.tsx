import {StyleSheet, View, ViewStyle} from 'react-native';
import React from 'react';
import {Dropdown} from 'react-native-element-dropdown';
import {DropdownProps} from 'react-native-element-dropdown/lib/typescript/components/Dropdown/model';
import {ThemeContext} from '@contexts/ThemeContext';
import {IColorType} from '@theme/color';

interface IDropdownPropsInterface extends DropdownProps<any> {
  data: any[];
  value: string;
  style?: ViewInterface & ViewStyle;
}

interface ViewInterface extends React.ComponentProps<typeof View> {
  color?: IColorType;
}

const DropDown = (props: IDropdownPropsInterface) => {
  const {
    data,
    value = 'value',
    labelField = 'label',
    valueField,
    onChange,
    ...rest
  } = props;
  const {selectedTheme} = React.useContext(ThemeContext);

  const [isFocus, setIsFocus] = React.useState(false);

  return (
    <Dropdown
      {...rest}
      style={[
        styles.dropdown,
        {
          borderColor: isFocus
            ? selectedTheme.colors.primary
            : selectedTheme.colors.border,
        },
      ]}
      placeholderStyle={{
        color:
          selectedTheme.colors[props.style?.color as any] ||
          selectedTheme.colors.text2,
      }}
      selectedTextStyle={{color: selectedTheme.colors.text2}}
      iconStyle={{tintColor: selectedTheme.colors.text2}}
      //   iconColor={selectedTheme.colors.text4}
      //   activeColor={selectedTheme.colors.secondary}
      //   dropdownPosition="auto"
      //   itemTextStyle={styles.selectedTextStyle}
      data={data}
      labelField={labelField}
      valueField={valueField}
      value={value}
      //   containerStyle={{borderRadius: 15}}
      onFocus={() => setIsFocus(true)}
      onBlur={() => setIsFocus(false)}
      onChange={item => {
        setIsFocus(false);
        onChange(item);
      }}
    />
  );
};

const styles = StyleSheet.create({
  dropdown: {
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 15,
    paddingHorizontal: 16,
    height: 48,
  },
  icon: {
    width: 25,
    height: 25,
  },
  selectedTextStyle: {
    fontSize: 16,
    textAlign: 'left',
    color: 'black',
  },
  btnTxt: {
    fontSize: 14,
  },
  btn: {
    borderRadius: 20,
    width: 136,
    alignSelf: 'center',
    height: 48,
  },
});

export default DropDown;
