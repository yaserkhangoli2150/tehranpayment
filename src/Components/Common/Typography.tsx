import {Text, TextStyle, TextProps} from 'react-native';
import React from 'react';
import {IFontSize, IFontWeight, typography} from '@theme/Typography';
import {ThemeContext} from '@contexts/ThemeContext';
import {moderateScale} from 'react-native-size-matters';
import {IColorType} from '@theme/color';

interface props extends TextProps {
  fontSize?: IFontSize;
  fontWeight?: IFontWeight;
  color?: IColorType;
  children?: any;
  style?: TextStyle[] | TextStyle;
}

const Typography = (props: props) => {
  const {selectedTheme} = React.useContext(ThemeContext);

  return (
    <Text
      {...props}
      allowFontScaling={false}
      style={[
        props.style,
        {
          color:
            selectedTheme.colors[props.color as IColorType & string] || 'black',
          fontSize:
            typography.fontSizes[props.fontSize as IFontSize] ||
            moderateScale(14),
          fontFamily:
            typography.fontWeight[props.fontWeight as IFontWeight] ||
            'Inter-Regular',
        },
      ]}>
      {props.children}
    </Text>
  );
};

export default Typography;
