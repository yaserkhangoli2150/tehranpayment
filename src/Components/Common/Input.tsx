import {
  View,
  StyleSheet,
  TextInput,
  Pressable,
  ViewStyle,
  KeyboardTypeOptions,
} from 'react-native';
import React, {memo} from 'react';
import {moderateScale} from 'react-native-size-matters';
import ITypography from './Typography';
import {ThemeContext} from '@contexts/ThemeContext';
import {IColorType} from '@theme/color';

interface ViewInterface extends React.ComponentProps<typeof View> {
  backgroundColor?: IColorType;
  borderColor?: IColorType;
  color?: IColorType;
}

type PropsType = {
  id?: string;
  value: string;
  label?: string;
  placeholder?: string;
  onChangeText: (text: string) => void;
  onEndIconPressed?: () => void;
  endIcon?: React.ReactNode;
  style?: ViewStyle;
  inputStyle?: ViewInterface & ViewStyle;
  secureTextEntry?: boolean;
  keyboardType?: KeyboardTypeOptions;
  error?: boolean;
  helperText?: string;
};

function Input({
  id,
  value,
  label,
  placeholder,
  onChangeText,
  endIcon,
  onEndIconPressed,
  style = {},
  inputStyle = {},
  secureTextEntry = false,
  keyboardType,
  error = false,
  helperText,
}: PropsType) {
  const {selectedTheme} = React.useContext(ThemeContext);

  const onIconPressed = () => {
    if (!!onEndIconPressed) onEndIconPressed();
  };

  return (
    <View style={{...styles.wrapper, ...style}}>
      {!!label && (
        <ITypography color="text2" style={styles.label}>
          {label}
        </ITypography>
      )}
      <View
        style={{
          ...styles.inputWrapper,
        }}>
        <TextInput
          id={id}
          style={{
            ...styles.input,
            ...inputStyle,
            backgroundColor:
              selectedTheme.colors[
                inputStyle?.backgroundColor as IColorType & string
              ] || selectedTheme.colors.background,
            borderColor: error
              ? selectedTheme.colors.error
              : selectedTheme.colors[
                  inputStyle?.borderColor as IColorType & string
                ] || selectedTheme.colors.border,
            color: error
              ? selectedTheme.colors.error
              : selectedTheme.colors.text2,
          }}
          placeholderTextColor={selectedTheme.colors.text3}
          placeholder={placeholder}
          onChangeText={onChangeText}
          value={value}
          secureTextEntry={secureTextEntry}
          keyboardType={keyboardType}
        />
        {!!endIcon && (
          <Pressable style={styles.icon} onPress={onIconPressed}>
            {endIcon}
          </Pressable>
        )}
      </View>
      {!!error && (
        <ITypography
          color="error"
          fontSize="xs"
          style={{marginTop: moderateScale(6)}}>
          {helperText}
        </ITypography>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    width: '100%',
  },
  label: {
    lineHeight: moderateScale(18),
    marginBottom: moderateScale(6),
  },
  inputWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: moderateScale(44),
  },
  input: {
    flex: 1,
    height: '100%',
    padding: moderateScale(12),
    borderWidth: 1,
    borderRadius: moderateScale(4),
  },
  icon: {
    width: moderateScale(20),
    height: moderateScale(20),
    marginEnd: moderateScale(12),
  },
});

export default memo(Input);
