import React, {useCallback, useEffect, useState} from 'react';
import {View} from 'react-native';
import {MainNavigationProp} from 'src/type';
import CoinDropDown, {CoinKeyType} from './CoinDropDown';
import {CoinInterface} from '@redux/Api/walletApiInterface';
import Typography from '@components/Common/Typography';
import CardView from '@components/Common/CardView';
import ChangeTheme from './ChangeTheme';
import Input from '@components/Common/Input';
import Swap from '@components/Common/Swap';

export default function LoginOrSignup({navigation}: MainNavigationProp) {
  const [firstCoin, setFirstCoin] = useState<CoinInterface>();
  const [secondCoin, setSecondCoin] = useState<CoinInterface>();
  const [coinInput, setCoinInput] = useState<string>('');
  const [swap, setSwap] = useState<boolean>(false);

  // Swap method
  useEffect(() => {
    if (firstCoin && secondCoin) {
      setFirstCoin(secondCoin);
      setSecondCoin(firstCoin);
    }
  }, [swap]);

  // Recalculate result when firstCoin, secondCoin or coinInput changes
  const swapMethod = useCallback(
    (coinInput = 1) => {
      if (firstCoin && secondCoin) {
        return (
          coinInput +
          ' ' +
          firstCoin?.baseSymbol +
          ' = ' +
          +coinInput * (+firstCoin?.priceQuote / +secondCoin?.priceQuote) +
          ' ' +
          secondCoin?.baseSymbol
        );
      }
    },
    [firstCoin, secondCoin, coinInput],
  );

  const toggleSwitch = useCallback(() => setSwap(prev => !prev), []);

  const onChangeCoin = useCallback((coinKey: CoinKeyType, value: string) => {
    console.log('\ncoinKey::', coinKey + '\nvalue::', JSON.parse(value));
    if (coinKey == 'FIST_COIN') {
      setFirstCoin(JSON.parse(value));
    } else {
      setSecondCoin(JSON.parse(value));
    }
  }, []);

  const onChangeText = useCallback((input: string) => {
    setCoinInput(input);
  }, []);

  return (
    <View style={{flex: 1, alignItems: 'center', padding: 24}}>
      <CardView style={{marginBottom: 24, width: '100%'}}>
        <ChangeTheme />
      </CardView>

      <CardView style={{marginBottom: 24, width: '100%'}}>
        <>
          <Typography color="text2" fontWeight="medium" fontSize="md">
            From:
          </Typography>
          <CoinDropDown
            key="firstCoin"
            coinKey="FIST_COIN"
            coin={{
              coinLabel: firstCoin?.baseSymbol,
              coinValue: JSON.stringify(firstCoin),
            }}
            onChangeValue={onChangeCoin}
          />
          <Input
            keyboardType="numeric"
            onChangeText={onChangeText}
            value={coinInput}
          />
        </>
      </CardView>
      <Swap onChange={toggleSwitch} value={swap} />
      <CardView style={{marginBottom: 24, marginTop: 24, width: '100%'}}>
        <Typography color="text2" fontWeight="medium" fontSize="md">
          To:
        </Typography>
        <CoinDropDown
          key="secondCoin"
          coinKey="SECOND_COIN"
          coin={{
            coinLabel: secondCoin?.baseSymbol,
            coinValue: JSON.stringify(secondCoin),
          }}
          onChangeValue={onChangeCoin}
        />
        <Typography color="text2" fontWeight="medium" fontSize="xs">
          {swapMethod(+coinInput)}
        </Typography>
      </CardView>
      {firstCoin && secondCoin && (
        <CardView style={{marginBottom: 24, width: '100%', borderWidth: 3}}>
          <Typography color="text2" fontWeight="medium" fontSize="lg">
            Result:
          </Typography>

          <Typography color="text2" fontWeight="medium" fontSize="md">
            {swapMethod()}
          </Typography>
        </CardView>
      )}
    </View>
  );
}
