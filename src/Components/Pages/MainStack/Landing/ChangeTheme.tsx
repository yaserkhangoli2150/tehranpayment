import React, {useEffect} from 'react';
import Switch from '@components/Common/Switch';
import {ThemeContext} from '@contexts/ThemeContext';
import {darkTheme, lightTheme} from '@theme/color';
import {View} from 'react-native';
import Typography from '@components/Common/Typography';

export default function ChangeTheme() {
  const {setThemeFunc, selectedTheme} = React.useContext(ThemeContext);
  const [isEnabled, setIsEnabled] = React.useState(
    selectedTheme.colorScheme == 'dark',
  );

  useEffect(() => {
    setThemeFunc(isEnabled ? darkTheme : lightTheme);
  }, [isEnabled]);

  const toggleSwitch = () => {
    setIsEnabled(previousState => !previousState);
  };

  return (
    <View style={{flexDirection: 'row'}}>
      <Typography color="text2" fontWeight="medium" fontSize="sm">
        {'Change Theme:'}
      </Typography>
      <Switch onValueChange={toggleSwitch} value={isEnabled} />
    </View>
  );
}
