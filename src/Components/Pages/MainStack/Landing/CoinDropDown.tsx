import {StyleSheet} from 'react-native';
import React, {memo, useEffect, useState} from 'react';
import DropDown from '@components/Common/DropDown';
import {useLazyGetCoinsQuery} from '@redux/Api/walletApi';
import {
  CoinInterface,
  MarketsApiInterface,
} from '@redux/Api/walletApiInterface';

interface CoinDropDownInterface {
  coinKey: CoinKeyType;
  coin: CoinsInterface | undefined;
  onChangeValue: (key: CoinKeyType, value: string) => void;
}

interface CoinsInterface {
  coinLabel?: string;
  coinValue?: string;
}

export type CoinKeyType = 'FIST_COIN' | 'SECOND_COIN';

const CoinDropDown = (props: CoinDropDownInterface) => {
  // Coins list
  const [coins, setCoins] = useState<CoinsInterface[]>();
  //
  const [coin, setCoin] = useState<CoinsInterface>();
  // Get coins list from api
  const [getCoinsQuery] = useLazyGetCoinsQuery();

  // Set default coin value
  useEffect(() => {
    if (props.coin) {
      setCoin(props.coin);
    }
  }, [props.coin]);

  // Cache the coins list after get coins
  useEffect(() => {
    getCoinsQuery({
      options: {Authorization: 'NONE'},
    })
      .unwrap()
      .then((res: MarketsApiInterface) => {
        const mappedCoins = mapCoinsToDropDown(res.data.slice(0, 10));
        setCoins(mappedCoins);
      })
      .catch(error => {});
  }, []);

  // Map markets api result to dropdown standard interface
  const mapCoinsToDropDown = (res: CoinInterface[]) => {
    const mappedCoins = res.map((coin: CoinInterface) => {
      return {
        coinLabel: coin.baseSymbol,
        coinValue: JSON.stringify(coin),
      };
    });
    return mappedCoins;
  };

  const onChange = (value: CoinsInterface) => {
    setCoin(value);
    props.onChangeValue(props.coinKey, value.coinValue as string);
  };

  return (
    <DropDown
      data={coins || []}
      labelField="coinLabel"
      valueField="coinValue"
      value={coin?.coinValue || ''}
      onChange={onChange}
      placeholder="Select coin"
    />
  );
};

export default memo(CoinDropDown);
