import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {MainStackNavigatorParamList} from 'src/type';
import Landing from '@components/Pages/MainStack/Landing';

const Stack = createNativeStackNavigator<MainStackNavigatorParamList>();

function AuthScreenGroup() {
  return (
    <Stack.Group>
      <Stack.Screen name="Landing" component={Landing} />
    </Stack.Group>
  );
}

export default AuthScreenGroup;
