import React, {useEffect} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MainScreenGroup from './MainScreenGroup';
import SplashScreen from 'react-native-splash-screen';

const Stack = createNativeStackNavigator();

const options = {
  headerShown: false,
  title: '',
};

const AppNavigator = () => {
  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 1500);
  }, []);

  return (
    <Stack.Navigator screenOptions={options}>
      {MainScreenGroup()}
    </Stack.Navigator>
  );
};

export default AppNavigator;
