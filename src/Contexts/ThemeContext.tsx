import React, {useLayoutEffect} from 'react';
import AsyncStorageConst from '@utils/AsyncStorage/asyncStorageConstants';
import asyncStorage from '@utils/AsyncStorage';
import {darkTheme, lightTheme} from '@theme/color';
import AsyncStorage from '@utils/AsyncStorage';
export const ThemeContext = React.createContext<any | null>(null);

export interface ThemeType {
  colorScheme: 'dark' | 'light';
  colors: {
    primary: string;
    secondary: string;
    background: string;
    background_modal: string;
    input: string;
    card: string;
    border: string;
    disable_outer: string;
    disable_inner: string;
    text1: string;
    text2: string;
    text3: string;
    text4: string;
    text5: string;
    error: string;
    confirm: string;
    warning: string;
    link: string;
    input2: string;
  };
}

export const ThemeProvider = (props: any) => {
  const [selectedTheme, setTheme] = React.useState<ThemeType>(darkTheme);

  useLayoutEffect(() => {
    (async () => {
      try {
        await asyncStorage.getStringData(AsyncStorageConst.theme).then(res => {
          setTheme(res == '' ? darkTheme : lightTheme);
        });
      } catch (e: any) {}
    })();
  }, []);

  const setThemeFunc = async (theme: ThemeType) => {
    try {
      await AsyncStorage.storeStringData(
        AsyncStorageConst.theme,
        theme.colorScheme,
      ).then(() => {
        setTheme(theme);
      });
    } catch {}
  };

  return (
    <ThemeContext.Provider value={{selectedTheme, setThemeFunc}}>
      {props.children}
    </ThemeContext.Provider>
  );
};
