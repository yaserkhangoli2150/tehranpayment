import {moderateScale} from 'react-native-size-matters';

export const typography = {
  fontSizes: {
    '3xs': moderateScale(8),
    '2xs': moderateScale(10),
    xs: moderateScale(12),
    sm: moderateScale(14),
    md: moderateScale(16),
    lg: moderateScale(18),
    xl: moderateScale(20),
    '2xl': moderateScale(24),
    '3xl': moderateScale(30),
    '4xl': moderateScale(36),
    '5xl': moderateScale(48),
    '6xl': moderateScale(60),
    '7xl': moderateScale(72),
    '8xl': moderateScale(96),
    '9xl': moderateScale(128),
  },
  fontWeight: {
    black: 'Inter-Black',
    bold: 'Inter-Bold',
    extraBold: 'Inter-ExtraBold',
    extraLight: 'Inter-ExtraLight',
    light: 'Inter-Light',
    medium: 'Inter-Medium',
    regular: 'Inter-Regular',
    semiBold: 'Inter-SemiBold',
    thin: 'Inter-Thin',
  },
};

export type IFontSize = keyof typeof typography.fontSizes;
export type IFontWeight = keyof typeof typography.fontWeight;
