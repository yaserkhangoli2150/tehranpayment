import React from 'react';
import AppNavigator from '@components/Navigator/AppNavigator';

export default function RootContainer() {
  return <AppNavigator />;
}
