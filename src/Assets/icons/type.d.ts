import {IColorType} from '@theme/color';
import {ViewStyle} from 'react-native';

export interface IconType {
  color?: IColorType;
  size?: number;
  style?: ViewStyle;
}
