import {ThemeContext} from '@contexts/ThemeContext';
import * as React from 'react';
import {View} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import {IconType} from './type';
import {IColorType} from '@theme/color';

function IconChange(props: IconType) {
  const {selectedTheme} = React.useContext(ThemeContext);

  return (
    <View style={props.style}>
      <Svg
        width={props.size || 24}
        height={props.size || 24}
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}>
        <Path
          d="M17.28 10.45L21 6.73l-3.72-3.72M3 6.73h18M6.72 13.549L3 17.269l3.72 3.72M21 17.27H3"
          stroke={
            selectedTheme.colors[props.color as IColorType] ||
            selectedTheme.colors.text1
          }
          strokeWidth={1.5}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </Svg>
    </View>
  );
}

export default IconChange;
