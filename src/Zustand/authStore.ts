import {UserProfileType} from '@redux/Slices/type';
import {create} from 'zustand';

type SignupStrategyType = 'EMAIL' | 'PHONE';
type PasswordStrategyType = 'CREATE' | 'FORGOT';
type AuthStorType = {
  signupStrategy: SignupStrategyType;
  passwordStrategy: PasswordStrategyType;
  mobileNumber?: string;
  email?: string;
  termsStatus: boolean;
  tempOtp: string;
  updateSignupStrategy: (strategy: SignupStrategyType) => void;
  updatePasswordStrategy: (strategy: PasswordStrategyType) => void;
  updateMobileNumber: (phone: string) => void;
  updateEmail: (email: string) => void;
  updateTermsStatus: (status: boolean) => void;
  updateTempOtp: (otp: string) => void;
};

export const useAuthStore = create<AuthStorType>(set => ({
  signupStrategy: 'PHONE',
  passwordStrategy: 'CREATE',
  mobileNumber: undefined,
  email: undefined,
  termsStatus: false,
  tempOtp: '',
  updateSignupStrategy: (signupStrategy: SignupStrategyType) =>
    set(state => ({...state, signupStrategy})),
  updatePasswordStrategy: (passwordStrategy: PasswordStrategyType) =>
    set(state => ({...state, passwordStrategy})),
  updateMobileNumber: (mobileNumber: string) =>
    set(state => ({...state, mobileNumber})),
  updateEmail: (email: string) => set(state => ({...state, email})),
  updateTermsStatus: (status: boolean) =>
    set(state => ({...state, termsStatus: status})),
  updateTempOtp: (otp: string) => set(state => ({...state, tempOtp: otp})),
}));
