import {create} from 'zustand';

type SignupProfileStoreType = {
  username?: string;
  date_of_birth?: string;
  password?: string;
  updateUsername: (value: string) => void;
  updateBirthDate: (value: string) => void;
};

export const useSignupProfileStore = create<SignupProfileStoreType>(set => ({
  username: undefined,
  date_of_birth: undefined,
  password: undefined,
  updateUsername: (username: string) => set(state => ({...state, username})),
  updateBirthDate: (date_of_birth: string) =>
    set(state => ({...state, date_of_birth})),
}));
