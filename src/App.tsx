/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {StyleSheet} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ThemeContext, ThemeProvider} from '@contexts/ThemeContext';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import RootContainer from './RootContainer';
import {Provider} from 'react-redux';
import store from '@redux/store';

function App(): JSX.Element {
  return (
    <ThemeProvider>
      <Main />
    </ThemeProvider>
  );
}

const Main = () => {
  const {selectedTheme} = React.useContext(ThemeContext);
  return (
    <SafeAreaProvider>
      <NavigationContainer
        theme={{
          dark: selectedTheme.colorScheme == 'dark',
          colors: {
            ...DefaultTheme.colors,
            primary: selectedTheme.colors.primary,
            background: selectedTheme.colors.background,
          },
        }}>
        <Provider store={store}>
          <RootContainer />
        </Provider>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default App;
