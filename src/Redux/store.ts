import {configureStore} from '@reduxjs/toolkit';
import walletSlice from './Slices/walletSlice';
import {appApi} from './Api/app';
import {setupListeners} from '@reduxjs/toolkit/query';

const store = configureStore({
  reducer: {
    wallet: walletSlice,
    [appApi.reducerPath]: appApi.reducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }).concat(appApi.middleware),
});

setupListeners(store.dispatch);
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
