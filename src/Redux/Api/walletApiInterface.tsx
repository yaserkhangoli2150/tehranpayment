export interface MarketsApiInterface {
  data: CoinInterface[];
  timestamp: Date;
}

export interface CoinInterface {
  baseId: string;
  baseSymbol: string;
  exchangeId: string;
  percentExchangeVolume: string;
  priceQuote: string;
  priceUsd: string;
  quoteId: string;
  quoteSymbol: string;
  rank: string;
  tradesCount24Hr: string;
  updated: number;
  volumeUsd24Hr: string;
}
