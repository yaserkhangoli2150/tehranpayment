export type IAxiosRTKQueryRequest = {
  queryParams?: any;
  pathParams?: any;
  body?: any;
  options: {
    Authorization: 'ACCESS_TOKEN' | 'REFRESH_TOKEN' | 'NONE';
    ['Content-Type']?: string;
  };
};

export type IAxiosRTKQueryResponse = {
  error: boolean;
  sstatusCode: number;
  message: string;
  data: object;
};

export type IAxiosThunkArg = {queryParams: any; pathParams: any; body: any};
