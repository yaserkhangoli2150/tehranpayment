import type {BaseQueryFn} from '@reduxjs/toolkit/query';
import axios from 'axios';
import type {AxiosRequestConfig, AxiosError} from 'axios';

const axiosBaseQuery =
  (
    {baseUrl}: {baseUrl: string} = {baseUrl: ''},
  ): BaseQueryFn<
    {
      url: string;
      method: AxiosRequestConfig['method'];
      data?: AxiosRequestConfig['data'];
      params?: AxiosRequestConfig['params'];
      headers?: AxiosRequestConfig['headers'];
    },
    unknown,
    unknown
  > =>
  async ({url, method, data, params, headers}, {getState, dispatch}) => {
    // Set loading
    try {
      let token = undefined;
      const Authorization: 'ACCESS_TOKEN' | 'REFRESH_TOKEN' | 'NONE' =
        headers?.Authorization;
      switch (Authorization) {
        case 'ACCESS_TOKEN':
          // Get accessToken from redux or storage
          break;

        default:
          token = undefined;
      }

      const result = await axios({
        url: 'https://api.coincap.io/v2/' + `${baseUrl}${url}`,
        method,
        data,
        params,
        headers: {
          ['Content-Type']: headers?.['Content-Type'] || 'application/json',
          // ['Authorization']: token ? `Bearer ${token}` : undefined,
        },
      });

      if (result.data.error) {
        throw new Error(result.data.message);
      }
      return {data: result.data};
    } catch (axiosError) {
      const err = axiosError as AxiosError;
      console.log('axiosError::', axiosError);
      console.log('Axios url::', url);
      console.log('AxiosError::', err.response?.status);
      if (err.response?.status === 401) {
        // User token expired...
      }
      notifyMessage(err.message);
      return {
        error: err.message,
      };
    } finally {
      // Disable loading
    }
  };

function notifyMessage(msg: string) {
  // We can show error here
}
export default axiosBaseQuery;
