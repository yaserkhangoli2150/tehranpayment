import {appApi} from './app';
import {IAxiosRTKQueryRequest} from './type';
import {MarketsApiInterface} from './walletApiInterface';

const walletApi = appApi.injectEndpoints({
  endpoints: builder => ({
    getCoins: builder.query<MarketsApiInterface, IAxiosRTKQueryRequest>({
      query: ({pathParams, options}) => {
        return {
          method: 'GET',
          headers: options,
          url: 'markets',
        };
      },
      providesTags: ['coins'], // Cache coins list until invalidate it
    }),
  }),
});

export const {useLazyGetCoinsQuery} = walletApi;
