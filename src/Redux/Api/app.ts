import {createApi} from '@reduxjs/toolkit/query/react';
import axiosBaseQuery from './axiosBaseQuery';
export const appApi = createApi({
  reducerPath: 'api/',
  refetchOnFocus: false,
  refetchOnReconnect: false,
  baseQuery: axiosBaseQuery({
    baseUrl: '',
  }),
  tagTypes: ['coins'],
  endpoints: () => ({}),
});
