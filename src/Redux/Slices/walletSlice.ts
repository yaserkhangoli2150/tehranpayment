import {createSlice} from '@reduxjs/toolkit';

type InitialState = {};

const initialState: InitialState = {};

const globalSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    // Redux actions
  },
  extraReducers(builder) {
    // Redux extra reducers
  },
});

export default globalSlice.reducer;
