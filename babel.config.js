module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          '@components': './src/Components',
          '@assets': './src/Assets',
          '@redux': './src/Redux',
          '@database': './src/Database',
          '@hooks': './src/Hooks',
          '@theme': './src/Theme',
          '@hoc': './src/HOC',
          '@contexts': './src/Contexts',
          '@utils': './src/Utils',
          '@Zustand': './src/Zustand',
        },
      },
    ],
    ['react-native-reanimated/plugin'],
  ],
};
